import { useState } from "react";
import { AppShell, Navbar, NavLink, Header, Title } from "@mantine/core";
import { IconShoppingCart, IconBook, IconChevronRight } from "@tabler/icons";
import Products from "./Products";
import ShoppingCart from "./ShoppingCart";

export const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

type Page = "products" | "cart";

function App() {
  const [selectedPage, setSelectedPage] = useState<Page>("products");

  return (
    <AppShell
      padding="md"
      navbar={
        <Navbar width={{ base: 300 }} height={500} p="xs">
          <NavLink
            label="Products"
            icon={<IconBook />}
            rightSection={<IconChevronRight />}
            onClick={() => setSelectedPage("products")}
          />
          <NavLink
            label="Shopping cart"
            icon={<IconShoppingCart />}
            rightSection={<IconChevronRight />}
            onClick={() => setSelectedPage("cart")}
          />
        </Navbar>
      }
      header={
        <Header height={60} p="xs">
          <Title order={2}>Boekzilla</Title>
        </Header>
      }
    >
      {selectedPage === "products" ? <Products /> : <ShoppingCart />}
    </AppShell>
  );
}

export default App;
