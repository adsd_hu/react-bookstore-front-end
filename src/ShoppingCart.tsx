import { useEffect, useState } from "react";
import { Container, Table, Group, Text } from "@mantine/core";
import { Product } from "./Products";
import { BACKEND_URL } from "./App";

interface ShoppingCartItem {
  product: Product;
  quantity: number;
}

interface IShoppingCart {
  items: [ShoppingCartItem];
  total: number;
}

function ShoppingCart() {
  const [cart, setCart] = useState<IShoppingCart>();

  function fetchProducts() {
    fetch(BACKEND_URL + "/cart")
      .then((res) => res.json())
      .then((json) => setCart(json));
  }

  useEffect(() => {
    fetchProducts();
  }, []);

  const rows = cart?.items.map((item: ShoppingCartItem) => (
    <tr key={item.product.id}>
      <td>{item.product.title}</td>
      <td>{item.quantity}</td>
      <td>€{item.quantity * item.product.price}</td>
    </tr>
  ));

  return (
    <Container>
      <Table>
        <thead>
          <tr>
            <th>Titel</th>
            <th>Aantal</th>
            <th>Totaal</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
      <Group
        style={{ padding: 5, marginRight: 30, marginTop: 5 }}
        position="right"
      >
        <Text size="xl">
          <b>Totaal: </b>
        </Text>
        <Text size="xl">€ {cart?.total}</Text>
      </Group>
    </Container>
  );
}

export default ShoppingCart;
