import { useEffect, useState } from "react";
import { Container, Table, Button, Alert } from "@mantine/core";
import { IconShoppingCart } from "@tabler/icons";
import { BACKEND_URL } from "./App";

export interface Product {
  id: number;
  price: number;
  title: string;
  author: {
    name: string;
  };
  inStock: boolean;
}

function Products() {
  const [products, setProducts] = useState<Product[]>([]);
  const [isAlertVisible, setAlertVisible] = useState(false);

  function fetchProducts() {
    fetch(BACKEND_URL + "/products")
      .then((res) => res.json())
      .then((json) => setProducts(json))
      .catch((err) => console.error(err));
  }

  function addProductToCart(id: number) {
    fetch(BACKEND_URL + "/cart/" + id, { method: "POST" })
      .then((res) => {
        if (res.status === 405) {
          setAlertVisible(true);
        }
        fetchProducts();
      })
      .catch((err) => console.error(err));
  }

  useEffect(() => {
    fetchProducts();
  }, []);

  const rows = products.map((product: Product) => (
    <tr key={product.id}>
      <td>{product.title}</td>
      <td>{product.author?.name}</td>
      <td>€{product.price}</td>
      <td>
        <Button
          leftIcon={<IconShoppingCart />}
          color="orange"
          size="md"
          onClick={() => {
            addProductToCart(product.id);
          }}
          disabled={!product.inStock}
        >
          {product.inStock ? "In winkelwagen" : "Uitverkocht"}
        </Button>
      </td>
    </tr>
  ));

  return (
    <Container>
      {isAlertVisible ? (
        <Alert title="Bummer!" color="red" withCloseButton>
          Het product is uitverkocht!
        </Alert>
      ) : null}
      <br />
      <Table>
        <thead>
          <tr>
            <th>Titel</th>
            <th>Auteur</th>
            <th>Prijs</th>
            <th>Op voorraad</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </Container>
  );
}

export default Products;
